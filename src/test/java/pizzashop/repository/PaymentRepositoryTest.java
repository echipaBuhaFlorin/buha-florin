package pizzashop.repository;

import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PaymentRepositoryTest {

    private PaymentRepository pr= new PaymentRepository();

    @Test
    void TC1_ECP() {
        Payment p = new Payment(3, PaymentType.CARD,100);
        pr.getAll().clear();
        pr.add(p);
        assert(pr.getAll().size()==1);
    }

    @Test
    void TC3_ECP() {
        Payment p = new Payment(-3, PaymentType.CARD,100);
        pr.getAll().clear();
        try{
            pr.add(p);}
        catch(Exception e){
            System.out.println(e);
        }

        assert(pr.getAll().size()==0);
    }

    @Test
    void TC5_ECP() {
        Payment p = new Payment(1, PaymentType.CASH,-200);
        pr.getAll().clear();
        try{
            pr.add(p);}
        catch(Exception e){
            System.out.println(e);
        }
        assert(pr.getAll().size()==0);
    }

    @Test
    void TC1_BVA() {
        Payment p = new Payment(3, PaymentType.CASH,100);
        pr.getAll().clear();
        pr.add(p);
        assert(pr.getAll().size()==1);
    }

    @Test
    void TC3_BVA() {
        Payment p = new Payment(9, PaymentType.CASH,100);
        pr.getAll().clear();

        try{
        pr.add(p);}
        catch(Exception e){
            System.out.println(e);
        }
        assert(pr.getAll().size()==0);
    }

    @Test
    void TC8_BVA() {
        Payment p = new Payment(1, PaymentType.CASH,0);
        pr.getAll().clear();
        try{
            pr.add(p);}
        catch(Exception e){
            System.out.println(e);
        }

        assert(pr.getAll().size()==0);
    }

    @Test
    void TC9_BVA() {
        Payment p = new Payment(1, PaymentType.CASH,1);
        pr.getAll().clear();
        pr.add(p);
        assert(pr.getAll().size()==1);
    }

}
