package pizzashop.repository;

import pizzashop.model.MenuData;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MenuRepository {
    private static String filename = "data/menu.txt";
    private List<MenuData> listMenu;

    public MenuRepository(){
    }

    private void readMenu() throws IOException {
        ClassLoader classLoader = MenuRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        this.listMenu= new ArrayList();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            while((line=br.readLine())!=null){
                MenuData menuItem=getMenuItem(line);
                listMenu.add(menuItem);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            br.close();
        }
    }

    private MenuData getMenuItem(String line){
        MenuData item=null;
        if (line==null|| line.equals("")) return null;
        StringTokenizer st=new StringTokenizer(line, ",");
        String name= st.nextToken();
        double price = Double.parseDouble(st.nextToken());
        item = new MenuData(name, 0, price);
        return item;
    }

    public List<MenuData> getMenu(){
        try {
            readMenu();//create a new menu for each table, on request
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listMenu;
    }

}
