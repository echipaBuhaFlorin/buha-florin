package pizzashop.repository;

import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

public class PaymentRepositoryTest1 {

    private PaymentRepository pr= new PaymentRepository();

    public PaymentRepositoryTest1() {
        T1();
        T2();
        T3();
        T4();
    }
    @Test
    public void T1(){
        Payment p = new Payment(1, PaymentType.CARD,100);
        pr.add(p);
    }

    @Test
    public void T2(){
        Payment p = new Payment(0, PaymentType.CARD,100);
        pr.add(p);
    }

    @Test
    public void T3(){
        Payment p = new Payment(3, PaymentType.CARD,100);
        pr.add(p);
    }

    @Test
    public void T4(){
        Payment p = new Payment(3, PaymentType.CARD,100);
        pr.add(p);
    }
}
