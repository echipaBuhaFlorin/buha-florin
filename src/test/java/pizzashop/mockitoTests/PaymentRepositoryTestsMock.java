package pizzashop.mockitoTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepositoryInMemory;

import static org.mockito.Mockito.mock;

public class PaymentRepositoryTestsMock {

    //aici testam functiile din repo

    PaymentRepositoryInMemory repo;
    Payment payment;

    @BeforeEach
    void setUp() {
        payment = mock(Payment.class);
        repo = new PaymentRepositoryInMemory();
    }

    @Test
    void addPayment() {

        Mockito.when(payment.getTableNumber()).thenReturn(7);
        Mockito.when(payment.getAmount()).thenReturn(1.2);

        Assertions.assertDoesNotThrow( ()-> repo.add(payment) );

    }

    @Test
    void addPaymentWithInvalidTable() {

        Mockito.when(payment.getTableNumber()).thenReturn(9);

        Assertions.assertThrows( ArithmeticException.class, () -> repo.add(payment));
    }

    @Test
    void addPaymentWithInvalidAmount() {

        Mockito.when(payment.getTableNumber()).thenReturn(7);
        Mockito.when(payment.getAmount()).thenReturn(-1.2);

        Assertions.assertThrows( ArithmeticException.class, () -> repo.add(payment));
    }
}
