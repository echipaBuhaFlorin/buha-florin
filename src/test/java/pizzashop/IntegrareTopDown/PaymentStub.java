package pizzashop.IntegrareTopDown;

import pizzashop.model.PaymentType;

public class PaymentStub {

    public PaymentStub() {
    }

    public int getTableNumber() {
        return 1;
    }

    public void setTableNumber(int tableNumber) { }

    public PaymentType getType() {
        return PaymentType.CARD;
    }

    public void setType(PaymentType type) { }

    public double getAmount() {
        return 1.1;
    }

    public void setAmount(double amount) { }

}
