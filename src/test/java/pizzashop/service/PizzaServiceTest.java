package pizzashop.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PizzaServiceTest {

    private PizzaService pizzaService;

    @Test
    public void TestIfPaymentsAreNull(){
        // testam primul if cand lista e null, deci sa returneze 0.0f
        PaymentRepository pr = new PaymentRepository("");
        MenuRepository mr = new MenuRepository();
        pizzaService = new PizzaService(mr, pr);

        assertEquals(0.0f, pizzaService.getTotalAmount(PaymentType.CARD));
    }

    @Test
    public void TestIfPaymentsAreEmpty(){
        PaymentRepository pr = new PaymentRepository("empty");
        MenuRepository mr = new MenuRepository();
        pizzaService = new PizzaService(mr, pr);

        assertEquals(0.0f, pizzaService.getTotalAmount(PaymentType.CARD));
    }

    @Test
    public void TestIfGetCorrectAmmountForCard() {
        PaymentRepository pr = new PaymentRepository("test");
        MenuRepository mr = new MenuRepository();
        pizzaService = new PizzaService(mr, pr);

        assertEquals(6.85, pizzaService.getTotalAmount(PaymentType.CARD));
    }

    @Test
    public void TestIfGetCorrectAmmountForCash() {
        PaymentRepository pr = new PaymentRepository("test");
        MenuRepository mr = new MenuRepository();
        pizzaService = new PizzaService(mr, pr);

        assertEquals(2.17, pizzaService.getTotalAmount(PaymentType.CASH));
    }
}
