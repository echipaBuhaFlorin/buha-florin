package pizzashop.mockitoTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import java.util.Arrays;

import static org.mockito.Mockito.mock;

public class PizzaServiceTestsMockito {

    PizzaService service;
    PaymentRepository paymentRepository;
    MenuRepository menuRepository;

    @BeforeEach
    void setUp() {
        paymentRepository = mock(PaymentRepository.class);
        menuRepository = mock(MenuRepository.class);
        service = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    void getAddPayment() {

        Mockito.doNothing().when(paymentRepository).add( mock(Payment.class) );
        Assertions.assertDoesNotThrow( ()-> service.addPayment(4, PaymentType.CARD, 1.2));

    }

    @Test
    void getAllPayments() {
        Mockito.when(paymentRepository.getAll()).thenReturn(Arrays.asList(
           new Payment(1,PaymentType.CARD,1.1),
           new Payment(2,PaymentType.CARD,1.1)
        ));
        Assertions.assertEquals(2, service.getPayments().size(), "eroare la payments");
    }
}
