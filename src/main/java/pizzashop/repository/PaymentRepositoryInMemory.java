package pizzashop.repository;

import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import java.util.ArrayList;
import java.util.List;

public class PaymentRepositoryInMemory extends PaymentRepository {

    public PaymentRepositoryInMemory(){
        super("empty");
    }

    public void add(Payment payment){
        if(payment.getTableNumber()<1 || payment.getTableNumber()>8)
        {
            throw new ArithmeticException("table number nor correct");
        }
        else
        if(payment.getAmount()<=0){
            throw new ArithmeticException("amaount not correct");
        }
        else
        {
            super.getAll().add(payment);
        }
    }

    public List<Payment> getAll(){
        return super.getAll();
    }
}
