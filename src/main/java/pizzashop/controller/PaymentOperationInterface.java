package pizzashop.controller;

public interface PaymentOperationInterface {
     void cardPayment();
     void cashPayment();
     void cancelPayment();
}
