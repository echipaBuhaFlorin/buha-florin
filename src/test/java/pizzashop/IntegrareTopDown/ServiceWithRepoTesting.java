package pizzashop.IntegrareTopDown;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepositoryInMemory;
import pizzashop.service.PizzaService;

import java.awt.*;

public class ServiceWithRepoTesting {

    PaymentRepositoryInMemory paymentRepository;
    MenuRepository menuRepository;
    PizzaService service;

    @BeforeEach
    void setUp() {
        paymentRepository = new PaymentRepositoryInMemory();
        menuRepository = new MenuRepository();
        service = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    void testAddPayment() {
        //cele doua cazuri de exceptie din repo
        Assertions.assertThrows( ArithmeticException.class , () -> service.addPayment(0, PaymentType.CARD, 1.1));
        Assertions.assertThrows( ArithmeticException.class , () -> service.addPayment(2, PaymentType.CARD, -1.1));
        //facem adaugarea si verificam daca e adaugat ce trebe
        service.addPayment(2, PaymentType.CARD, 1.1);
        Assertions.assertEquals(1, service.getPayments().size(), "nu s-a adaugat un payment nou");
    }

    @Test
    void testGetAllPayments() {
        Assertions.assertEquals(0, service.getPayments().size(), "size e diferit de ce ar trebui");
        service.addPayment(2, PaymentType.CARD, 1.1);
        Assertions.assertEquals(1, service.getPayments().size(), "size e diferit de ce ar trebui");
    }
}
